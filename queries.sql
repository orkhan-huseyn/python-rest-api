-- enable foreign key constraints
PRAGMA foreign_keys = ON;

-- Creating tables 
CREATE TABLE users (
    'id' INT PRIMARY KEY,
    'full_name' VARCHAR,
    'username' VARCHAR,
    'password' VARCHAR
);

CREATE TABLE tasks (
    'id' INT PRIMARY KEY,
    'title' VARCHAR,
    'status' INT, -- 0 new, 1 in-progress, 2 completed,
    'user_id' INT,
    FOREIGN KEY (user_id) REFERENCES users(id)
);

-- Inserting data to users table
INSERT INTO users ('id', 'full_name', 'username', 'password') VALUES (1, 'İsrafil Vəliyev', 'isko', 'isko_1234');
INSERT INTO users ('id', 'full_name', 'username', 'password') VALUES (2, 'Fuad Mansurov', 'fuma', 'fuma_1234');
INSERT INTO users ('id', 'full_name', 'username', 'password') VALUES (3, 'Səbuhi Zərbəliyev', 'sabo', 'sabo_1234');

-- Inserting data to tasks table
INSERT INTO tasks ('id', 'title', 'status', 'user_id') VALUES (1, 'Do your homework', 0, 3);
INSERT INTO tasks ('id', 'title', 'status', 'user_id') VALUES (2, 'Wash dishes', 0, 3);
INSERT INTO tasks ('id', 'title', 'status', 'user_id') VALUES (3, 'Get sonra gel', 0, 2);
INSERT INTO tasks ('id', 'title', 'status', 'user_id') VALUES (4, 'Nese ele', 0, 2);
INSERT INTO tasks ('id', 'title', 'status', 'user_id') VALUES (5, 'Make your bed', 0, 1);
INSERT INTO tasks ('id', 'title', 'status', 'user_id') VALUES (6, 'What can I do? Sometimes', 0, 1);
INSERT INTO tasks ('id', 'title', 'status', 'user_id') VALUES (7, 'It is football', 0, 1);

-- Selecting data by condition
SELECT * FROM tasks WHERE user_id=1 AND status=2;

-- Update rows
UPDATE tasks SET status=2 WHERE id=6;
UPDATE tasks SET status=1 WHERE id=3;

-- delete some users
DELETE FROM users;

-- Select number of completed tasks
SELECT COUNT(id) FROM tasks WHERE status=2;

-- Group tasks by status and show count
SELECT 
    CASE status
        WHEN 0 THEN 'New task'
        WHEN 1 THEN 'In progress'
        ELSE 'Completed'
    END
    as task_category, 
    COUNT(id) as number_of_tasks
FROM tasks
GROUP BY status;

-- join tables
SELECT tasks.id, title, CASE status
        WHEN 0 THEN 'New task'
        WHEN 1 THEN 'In progress'
        ELSE 'Completed'
    END, users.full_name 
FROM tasks JOIN users ON users.id=tasks.user_id; 

-- how many tasks each user have
SELECT users.username, COUNT(tasks.id)
FROM tasks
JOIN users ON users.id=tasks.user_id
GROUP BY user_id;