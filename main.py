from flask import Flask, request

app = Flask(__name__)

todo_database = [
    {
        'id': 1,
        'title': 'Paltarları yu',
        'completed': False
    },
    {
        'id': 2,
        'title': 'Dərslərini oxu',
        'completed': True
    },
    {
        'id': 3,
        'title': 'Evi təmizlə',
        'completed': False
    }
]


@app.get('/todos')
def get_all_todos():
    return todo_database


@app.get('/todos/<id>')
def get_todo_by_id(id):
    for todo in todo_database:
        if todo['id'] == int(id):
            return todo
    return dict()


@app.post('/todos')
def create_todo():
    todo = dict(request.json)
    todo_database.append(todo)
    return todo


@app.put('/todos/<id>')
def update_todo(id):
    todo = dict(request.json)
    for i in range(len(todo_database)):
        if todo_database[i]['id'] == int(id):
            todo_database[i]['title'] = todo['title']
            todo_database[i]['completed'] = todo['completed']
            break
    return todo


@app.delete('/todos/<id>')
def delete_todo(id):
    for todo in todo_database:
        if todo['id'] == int(id):
            todo_database.remove(todo)
            break
    return dict()


app.run('192.168.1.46', 55554)
